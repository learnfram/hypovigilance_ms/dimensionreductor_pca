FROM openjdk:16-slim-buster

COPY requirements.txt ./

# Install Python
RUN apt-get update && apt-get install --yes python3 python3-pip

RUN pip3 install --no-cache-dir -r requirements.txt

ENV PYTHONPATH "${PYTHONPATH}:/opt/dimension_reductor_pca/src/"

COPY src /opt/dimension_reductor_pca/src/
ENTRYPOINT ["python3", "opt/dimension_reductor_pca/src/main.py"]
