#!/usr/bin/env python3

import logging
import uuid
from typing import List

import pandas as pd
from learnfram.application.Application import Application
from learnfram.services.DataComponent import DataComponent
from sklearn.decomposition import PCA


class Service(DataComponent):

    variables: List[str] = ["nd"]
    component_type = "dimension_reductor_pca"

    def __init__(self, id: int, nd: int):
        super(Service, self).__init__(id)
        self.nd = nd

    def run(self):
        logging.info(f"Starting...")

        df = pd.DataFrame.from_dict(self.data)

        pca = PCA(self.nd)
        principal_components = pca.fit_transform(df.to_numpy())
        new_df = pd.DataFrame(principal_components, columns=df.columns)

        file_name = "dimension_reductor_{}.csv".format(uuid.uuid1())

        self.send(file_name, new_df.to_dict())


if __name__ == "__main__":
    Application.start(Service)
